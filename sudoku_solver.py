grid_sudoku =[
    [5,3,0,0,7,0,0,0,0],
    [6,0,0,1,9,5,0,0,0],
    [0,9,8,0,0,0,0,6,0],
    [8,0,0,0,6,0,0,0,3],
    [4,0,0,8,0,3,0,0,1],
    [7,0,0,0,2,0,0,0,6],
    [0,6,0,0,0,0,2,8,0],
    [0,0,0,4,1,9,0,0,5],
    [0,0,0,0,8,0,0,0,0]
    ]

def print_grid():
    for x in range(9):
        print(grid_sudoku[x])

def switch(value):

    switcher = {
        0: 0,
        1: 0,
        2: 0,
        3: 3,
        4: 3,
        5: 3,
        6: 6,
        7: 6,
        8: 6
    }

    return switcher.get(value)

def check_box(x,y):

    return (switch(x),switch(y))

def possible(x,y,number):
    
    for i in range(0,9):
        if grid_sudoku[x][i] == number:
            return False
    for j in range(0,9):
        if grid_sudoku[j][y] == number:
            return False

    (pos_x,pos_y) = check_box(x,y)

    for i in range(0,3):
        for j in range(0,3):
            if grid_sudoku[pos_x + i][pos_y + j] == number:
                return False
    return True

        
def solve():
    for x in range(9):
        for y in range(9):
            if grid_sudoku[x][y] == 0:
                for number in range(1,10):
                    if possible(x,y,number) :
                        grid_sudoku[x][y] = number
                        solve()
                        grid_sudoku[x][y] = 0
                return
    print_grid()
    print("")
    input("Uma das possiveis soluções. [Enter] for more if possible.") 
    print("")


#print_grid()
solve()
